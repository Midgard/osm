# OpenStreetMap resources

## JOSM styles
* [Style](https://framagit.org/Midgard/osm/raw/master/josm_style_grb.mapcss) for AIV GRB buildings import ([OSM wiki](https://wiki.openstreetmap.org/wiki/AIV_GRB_building_import))
* [Pipelines: colour per substance, show ref](https://framagit.org/midgard/osm/raw/master/pipelines.mapcss)

## JOSM tagging presets
* [Cables and pipelines in Belgium](https://framagit.org/midgard/osm/raw/master/belgium_cables_pipes_josm.xml)
