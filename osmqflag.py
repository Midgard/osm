#!/usr/bin/env python3

import sys
import re
import requests


__version__ = "1.0"


OVERRIDES = {
	"EU": ("Q122482", "https://commons.wikimedia.org/wiki/Special:FilePath/Flag%20of%20Europe.svg")
}

# https://query.wikidata.org/#select%20%3FcountryIso%20%3Fflag%20%3FflagImage%20where%20%7B%0A%20%20%3Fcountry%20wdt%3AP297%20%3FcountryIso%3B%20wdt%3AP163%20%3Fflag.%0A%20%20%3Fflag%20wdt%3AP18%20%3FflagImage.%0A%7D
def get_flags():
	r = requests.get(
		"https://query.wikidata.org/sparql?query=select%20%3FcountryIso%20%3Fflag%20%3FflagImage%20where%20%7B%0A%20%20%3Fcountry%20wdt%3AP297%20%3FcountryIso%3B%20wdt%3AP163%20%3Fflag.%0A%20%20%3Fflag%20wdt%3AP18%20%3FflagImage.%0A%7D",
		headers={
			"Accept": "application/json",
			"User-Agent":
				f"OSMQFlag/{__version__} (https://www.wikidata.org/wiki/User:M!dgard/OSMQFlag) "
				f"python-requests/{requests.__version__}"}
	)
	r.raise_for_status()
	result = r.json()["results"]["bindings"]

	return {
		**{
			x["countryIso"]["value"]: (only_qcode(x["flag"]["value"]), https(x["flagImage"]["value"]))
			for x in result
		},
		**OVERRIDES
	}


def only_qcode(url):
	m = re.fullmatch(r"https?://www.wikidata.org/entity/(Q[0-9]+)", url)
	assert m
	return m.group(1)


def https(url):
	m = re.fullmatch(r"https?://(.+)", url)
	return "https://" + (m.group(1) if m else url)


def main():
	global FLAGS

	if sys.stdout.isatty():
		print("How to use: redirect this script's output to a file. In most environments this can be done by appending this to the command: > filename.l0", file=sys.stderr)
		print("Then paste Level0 data as input. For each element with the tag country=* this script will try to add the corresponding flag:wikidata=*.", file=sys.stderr)
		sys.exit(1)

	print("Downloading flags...", file=sys.stderr)
	FLAGS = get_flags()

	if sys.stdin.isatty():
		print("Paste OSM elements in Level0 format here. For each element with the tag country=* this script will try to add the corresponding flag:wikidata=*.\n", file=sys.stderr)

	for line in sys.stdin:
		print(line, end="")

		m = re.fullmatch(r"  country = ([A-Z]{2})", line.rstrip())
		if not m:
			continue

		iso_code = m.group(1)
		try:
			flag = FLAGS[iso_code]
			print(f"  flag:wikidata = {flag[0]}")
			print(f"Adding {flag[0]:10} for {iso_code} (image: {flag[1]})", file=sys.stderr)
		except KeyError as e:
			print(f"!!! Could not add flag for country={iso_code}", file=sys.stderr)
			print(f"    {e}", file=sys.stderr)

if __name__ == '__main__':
	main()
